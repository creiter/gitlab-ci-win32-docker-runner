# GitLab CI Windows Runner using Docker

## Docker images

The docker images inherit from a base image that matches the runner host so we
can use process isolation instead of hyper-v. If you need something in the base
images or a custom image it's recommended to contribute it here so it will be
kept in sync with the host for everyone.

**msys2-mingw64**: `registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/msys2-mingw64`

* MSYS2 under C:/msys64
  * installed: mingw-w64-x86_64-toolchain, git
* mingw64 toolchain
* chocolatey  (`choco` in PATH)

**msys2-mingw32**: `registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/msys2-mingw32`

* MSYS2 under C:/msys64
  * installed: mingw-w64-i686-toolchain, git
* chocolatey  (`choco` in PATH)

**vs2017**: `registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/vs2017`

* Visual Studio 2017
* vswhere (`vswhere` in PATH)
* git (`git`in PATH)
* ninja (`ninja` in PATH)
* Python 3.9 (`python` in PATH)
* chocolatey  (`choco` in PATH)

## Runner Setup

* Install Windows 10 2004
* Enable admin account and set an admin passwort (for ansible access)
* Disable Defender via group policy (gpedit.msc)
* Disable Search Index service via services.msc
* Run setup for ansible: https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html
* Run ansible scripts
* Start docker desktop:
  * settings: "start when you log in"
  * switch to windows containers
  * daemon: add `"exec-opts": ["isolation=process"]`
* Reboot: everything should start automatically