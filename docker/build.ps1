Get-Content Dockerfile.msys2-base | docker build -t registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/msys2-base -
Get-Content Dockerfile.msys2-mingw-base | docker build -t registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/msys2-mingw-base -
Get-Content Dockerfile.msys2-mingw32 | docker build -t registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/msys2-mingw32 -
Get-Content Dockerfile.msys2-mingw64 | docker build -t registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/msys2-mingw64 -
Get-Content Dockerfile.vs2017 | docker build -t registry.gitlab.gnome.org/creiter/gitlab-ci-win32-docker-runner/vs2017 -